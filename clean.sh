#!/bin/bash

rm -rf alpine-icecap-*
docker rm $(docker stop $(docker ps -a -q --filter ancestor=alpine-icecap-host --format="{{.ID}}"))
docker rm $(docker stop $(docker ps -a -q --filter ancestor=alpine-icecap-guest --format="{{.ID}}"))
docker rmi alpine-icecap-host
docker rmi alpine-icecap-guest
rm host.ext2.qcow2